// Load environment variables from .env file
require("dotenv").config();

// Import required modules and libraries
const express = require("express");
const app = express();
const path = require("path");
const db = require("./config/mongoose");
const expressLayouts = require("express-ejs-layouts");
const cookieParser = require("cookie-parser");
const session = require("express-session");
const passport = require("passport");
const passportLocal = require("./config/passport-local-strategy");
const mongoStore = require("connect-mongo")(session);
const flash = require("connect-flash");
const customMiddleware = require("./config/middleware");

// Helper function to set up additional view helpers
require("./config/view-helper")(app);

// Set EJS as the view engine
app.set("view engine", "ejs");

// Middleware to parse incoming request data
app.use(express.urlencoded());

// Middleware for parsing cookies
app.use(cookieParser());

// Enable using EJS layouts
app.use(expressLayouts);
app.set("layout", "layout");
app.set("layout extractScripts", true);
app.set("layout extractStyles", true);

// Middleware for SASS to CSS compilation
const sassMiddleware = require("node-sass-middleware");
app.use(
  sassMiddleware({
    src: path.join(__dirname, "./assets", "sass"),
    dest: path.join(__dirname, "./assets", "css"),
    debug: true,
    outputStyle: "extended",
    prefix: "/css",
  })
);

// Serve static assets from the 'assets' directory
app.use(express.static("./assets"));

// Setup session middleware with MongoDB store
app.use(
  session({
    name: "employee",
    secret: process.env.SESSION_SECRET,
    saveUninitialized: false,
    resave: false,
    cookie: {
      maxAge: 1000 * 60 * 100,
    },
    store: new mongoStore(
      {
        mongooseConnection: db,
        autoRemove: "disabled",
      },
      function (error) {
        console.log(error || "connect-mongodb setup ok");
      }
    ),
  })
);

// Initialize Passport authentication
app.use(passport.initialize());
app.use(passport.session());

// Custom middleware for logging request details
app.use((req, res, next) => {
  console.log("URL:", req.originalUrl);
  console.log("Method:", req.method);
  console.log("Params:", req.params);
  console.log("Headers:", req.headers);
  console.log("Body:", req.body);
  next();
});

// Middleware to set the authenticated user in the request object
app.use(passport.setAuthenticatedUser);

// Middleware for flash messages
app.use(flash());
app.use(customMiddleware.setFlash);

// Route for handling homepage and other routes
app.use("/", require("./routes/index"));

// Start the server and listen on the specified port
const port = 8000;
app.listen(port, (error) => {
  if (error) {
    console.log("Error:", error);
    return;
  }
  console.log(`App listening on port: ${port}`);
});
