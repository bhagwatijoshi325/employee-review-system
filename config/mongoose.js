const mongoose = require("mongoose");

main().catch((err) => console.log(err));
console.log(process.env.DB);
async function main() {
  // await mongoose.connect(`mongodb://127.0.0.1/employee_review`);
  await mongoose.connect(
    process.env.DB
  );
  console.log(`Connected to DB : employee_review`);
}

const db = mongoose.connection;

module.exports = db;
