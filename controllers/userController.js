const User = require("../models/user");
const fs = require("fs");
const path = require("path");

// Function to render the sign-in page
module.exports.signIn = (req, res) => {
  // If the user is already authenticated, redirect to the appropriate dashboard
  if (req.isAuthenticated()) {
    if (req.user.role == "Admin") return res.redirect("/user/admin/home");
    else return res.redirect("/user/employee/home");
  }
  // If the user is not authenticated, render the sign-in page
  return res.render("sign-in", {
    title: "Sign In",
  });
};

// Function to handle user login
module.exports.loginUser = (req, res) => {
  // Flash a success message to indicate successful login
  req.flash("success", "Logged In Successfully");
  // Redirect the user to the appropriate dashboard based on their role
  if (req.user.role == "Admin") return res.redirect("/user/admin/home");
  else return res.redirect("/user/employee/home");
};

// Function to render the sign-up page
module.exports.signUp = (req, res) => {
  // If the user is already authenticated, redirect to the appropriate dashboard
  if (req.isAuthenticated()) {
    if (req.user.role == "Admin") return res.redirect("/user/admin/home");
    else return res.redirect("/user/employee/home");
  }
  // If the user is not authenticated, render the sign-up page
  return res.render("sign-up", {
    title: "Sign Up",
  });
};

// Function to handle user registration
module.exports.createUser = (req, res) => {
  // Check if the provided password and confirm_password match
  if (req.body.password != req.body.confirm_password) {
    // If they don't match, flash a warning message and redirect back to the sign-up page
    req.flash("warning", "Password and Confirm Password do not match");
    return res.redirect("back");
  }

  // Check if a user with the same email already exists in the database
  User.findOne({ email: req.body.email }, function (error, user) {
    if (error) {
      // If there is an error while querying the database, flash an error message and redirect back to the sign-up page
      console.log(error);
      req.flash("error", "User could not be created");
      return res.redirect("back");
    }
    if (!user) {
      // If no user with the same email exists, create a new user
      User.create(req.body, function (error, user) {
        if (error) {
          // If there is an error while creating the user, flash an error message and redirect back to the sign-up page
          console.log(error);
          req.flash("error", "User could not be created");
          return res.redirect("back");
        } else {
          // If the user is created successfully, flash a success message and redirect to the sign-in page
          req.flash("success", "User Signed Up Successfully");
          return res.redirect("/user/sign-in");
        }
      });
    } else {
      // If a user with the same email already exists, flash a warning message and redirect back to the sign-up page
      req.flash("warning", "User already exists with this Email");
      return res.redirect("back");
    }
  });
};

// Function to handle user sign-out
module.exports.signOut = (req, res) => {
  // Call the logout method provided by Passport to log out the user
  req.logout(function (error) {
    if (error) {
      // If there is an error while logging out, log the error, flash an error message, and redirect back to the current page
      console.log(error);
      req.flash("error", "User could not be signed out");
      return res.redirect("back");
    }
    // If the user is successfully logged out, flash a success message and redirect to the sign-in page
    req.flash("success", "You have logged out");
    return res.redirect("/user/sign-in");
  });
};
