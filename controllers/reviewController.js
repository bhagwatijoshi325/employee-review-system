const User = require("../models/user");
const Review = require("../models/review");
const fs = require("fs");
const path = require("path");

// Controller to render the feedback form
module.exports.feedbackForm = async (req, res) => {
  try {
    // Find the review related to the specific user and reviewer
    let review = await Review.findOne({
      for_user: req.body.for_user,
      from_user: req.user.id,
    }).populate("for_user");

    // Render the feedback form with the review data
    return res.render("feedback-form", {
      layout: false,
      review: review,
    });
  } catch (error) {
    console.log(error);
    return "";
  }
};

// Controller to view feedback for a particular user
module.exports.viewFeedback = async (req, res) => {
  try {
    // Find the review related to the specific user and reviewer
    let review = await Review.findOne({
      for_user: req.body.for_user,
      from_user: req.user.id,
    }).populate("for_user");

    // Render the view feedback page with the review data
    return res.render("view-feedback", {
      layout: false,
      review: review,
    });
  } catch (error) {
    console.log(error);
    return "";
  }
};

// Controller to update feedback for a particular review
module.exports.updateFeedback = async (req, res) => {
  try {
    // Determine the status of the review based on feedback content
    let reviewStatus = "Submitted";
    if (!req.body.feedback) reviewStatus = "Pending";

    // Update the review with the new feedback and status
    let review = await Review.findByIdAndUpdate(req.body.review_id, {
      feedback: req.body.feedback,
      reviewStatus: reviewStatus,
    });

    // Respond with a success message and relevant data
    return res.status(200).json({
      data: {
        previousStatus: review.reviewStatus,
        reviewStatus: reviewStatus,
        employee: {
          id: req.body.for_user,
          name: req.body.for_user_name,
          email: req.body.for_user_email,
        },
      },
      message: "Feedback Updated Successfully",
    });
  } catch (error) {
    console.log(error);
    // Respond with an error message if the update failed
    return res.status(500).json({
      message: "Feedback could not be Updated",
    });
  }
};
