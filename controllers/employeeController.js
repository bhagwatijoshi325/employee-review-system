const User = require("../models/user");
const fs = require("fs");
const path = require("path");
const Review = require("../models/review");

// Controller to render the employee home page
module.exports.home = async (req, res) => {
  try {
    // Retrieve all users from the database
    let users = await User.find();

    // Retrieve all pending reviews for the logged-in user
    let pending_reviews = await Review.find({
      from_user: req.user.id,
      reviewStatus: "Pending",
    }).populate("for_user");

    // Retrieve all submitted reviews for the logged-in user
    let submitted_reviews = await Review.find({
      from_user: req.user.id,
      reviewStatus: "Submitted",
    }).populate("for_user");

    // Render the employee home page with the retrieved data
    return res.render("employee", {
      title: "Home",
      users: users,
      pending_reviews: pending_reviews,
      submitted_reviews: submitted_reviews,
    });

  } catch (error) {
    // Handle any errors by rendering the employee home page with empty data
    return res.render("employee", {
      title: "Home",
      users: [],
      pending_reviews: [],
      submitted_reviews: [],
    });
  }
};
