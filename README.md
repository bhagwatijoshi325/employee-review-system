# Employee-Review-System web application

An application that allows employees to submit feedback toward each other’s performance.

## Key Functionalities:

- Sign In:-
    - Sign in Using email id and password
    - For Admin:-
        - email_id- admin@gmail.com
        - password- admin12345

- Sign Up:-
    - Sign up Using email id
    - New sign-up user is an employee

- Admin view:- <br />
    - Add/remove/update/view employees
    - Edit Roles (Employee or Admin)
    - Add/update/view performance reviews
    - Assign employees to participate in another employee's performance review
    - Admin can make an employee an admin

- Employee view:-
    - List of performance review requiring feedback
    - Submit feedback


## Technologies Employed:

- Node.js: Provides the runtime environment for the web application.
- CSS: Facilitates visually pleasing and responsive design.
- EJS: Enables dynamic view rendering, enhancing the presentation of habit-related data.
- MongoDB: A NoSQL database used for dependable data storage and retrieval.
- Express: A web application framework for managing HTTP requests and responses.
- Mongoose: A sophisticated MongoDB object modeling tool for database interaction.



## Requirements

For development, you will only need Node.js (16+), a node global package (Npm), and mongoDB atlas URI or locally installed mongodb server.

### Node
- #### Node installation on Windows

  Just go on [official Node.js website](https://nodejs.org/) and download the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Node installation on Ubuntu

  You can install nodejs and npm easily with apt install, just run the following commands.

      $ sudo apt install nodejs
      $ sudo apt install npm

- #### Other Operating Systems
  You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).

  If the installation was successful, you should be able to run the following command.

    $ node --version <br>
    V16.xx.xx

    $ npm --version <br>
    x.xx.xx

  If you need to update `npm`, you can make it using `npm` !Cool right? After running the following command, just open again the command line and be happy.

    $ npm install npm -g


## Install Project

    $ git clone https://gitlab.com/bhagwatijoshi325/employee-review-system.git
    $ cd habit-maker
    $ npm install

## Running the project on development
    # Set up .env file in the root, format is given in the .env.example
    
    $ npm run start:dev


## Folder Structure
```
[Employee Review System]
│   .env
│   .gitignore
│   index.js
│   package-lock.json
│   package.json
│   README.md
│   seeder.js
│
├───assets
│   ├───css
│   │       admin.css
│   │       authentication.css
│   │       employee.css
│   │       form.css
│   │       header.css
│   │       layout.css
│   │       modal.css
│   │
│   ├───js
│   │       admin.js
│   │       datatable.js
│   │       layout.js
│   │
│   └───sass
│           admin.scss
│           authentication.scss
│           employee.scss
│           form.scss
│           header.scss
│           layout.scss
│           modal.scss
│
├───config
│       middleware.js
│       mongoose.js
│       passport-local-strategy.js
│       view-helper.js
│
├───controllers
│       adminController.js
│       employeeController.js
│       reviewController.js
│       userController.js
│
├───models
│       review.js
│       user.js
│
├───routes
│       admin.js
│       employee.js
│       index.js
│       review.js
│       user.js
│
└───views
        add-user.ejs
        admin.ejs
        all-users.ejs
        assign-reviewers.ejs
        edit-feedback.ejs
        edit-user.ejs
        employee.ejs
        feedback-form.ejs
        header.ejs
        layout.ejs
        pending-reviews.ejs
        sign-in.ejs
        sign-up.ejs
        submitted-reviews.ejs
        view-feedback.ejs
        view-reviewers.ejs
```


## Support

Contributions, bug reports, and feature requests are welcome. Feel free to fork the repository and submit pull requests to contribute to the project.